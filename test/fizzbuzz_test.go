/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   fizzbuzz_test.go                                                         */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 06:15:14 by 4nzar                                    */
/*   Updated: 20/03/2023 09:02:06 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/4nzar/workshop-fizzbuzz/src/api/fizzbuzz"
)

func Test_fizzbuzz_simple(t *testing.T) {
	var tests = []struct {
		int1   int
		int2   int
		limit  int
		str1   string
		str2   string
		expect string
	}{
		{int1: 3, int2: 5, limit: 5, str1: "Fizz", str2: "Buzz", expect: "12Fizz4Buzz"},
		{int1: 3, int2: 5, limit: 6, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz"},
		{int1: 3, int2: 5, limit: 7, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz7"},
		{int1: 3, int2: 5, limit: 8, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78"},
		{int1: 3, int2: 5, limit: 9, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78Fizz"},
		{int1: 3, int2: 5, limit: 10, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78FizzBuzz"},
		{int1: 3, int2: 5, limit: 11, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78FizzBuzz11"},
		{int1: 3, int2: 5, limit: 12, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78FizzBuzz11Fizz"},
		{int1: 3, int2: 5, limit: 13, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78FizzBuzz11Fizz13"},
		{int1: 3, int2: 5, limit: 15, str1: "Fizz", str2: "Buzz", expect: "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz"},
	}

	for _, test := range tests {
		got := fizzbuzz.FizzBuzz(test.int1, test.int2, test.limit, test.str1, test.str2)
		assert.NotEmpty(t, got)
		assert.Equal(t, test.expect, strings.Join(got, ""))
	}
}
