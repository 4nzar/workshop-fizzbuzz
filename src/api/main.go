/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   main.go                                                                  */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 05:50:56 by 4nzar                                    */
/*   Updated: 20/03/2023 18:50:14 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	swagger_files "github.com/swaggo/files"
	gin_swagger "github.com/swaggo/gin-swagger"

	"gitlab.com/4nzar/workshop-fizzbuzz/docs"
	"gitlab.com/4nzar/workshop-fizzbuzz/src/api/fizzbuzz"
	"gitlab.com/4nzar/workshop-fizzbuzz/src/api/health"
	"gitlab.com/4nzar/workshop-fizzbuzz/src/api/statistics"
)

const (
	VERSION = 1
)

// @title Technical test - FizzBuzz REST API (Remix)
// @host localhost:8080
// @version 1.0
// @Schemes http
// @BasePath /api/v1/
// @Accept json
// @Produce json
// @description The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by 'fizz', all multiples of 5 by 'buzz', and all multiples of 15 by 'fizzbuzz'. This server exposes two main endpoint, one to return the fizz-buzz string given some parameters and the other to return stats about the most used request.
func main() {
	host := flag.String("host", "127.0.0.1", "ip address")
	port := flag.String("port", "8080", "port")
	secure := flag.Bool("secure", false, "force https")
	certificate := flag.String("certificate", "", "force https")
	key := flag.String("key", "", "force https")
	flag.Parse()

	docs.SwaggerInfo.BasePath = ""

	router := gin.Default()
	router.GET("/", health.HealthCheck)
	router.GET("/docs/*any", gin_swagger.WrapHandler(swagger_files.Handler))

	// {{URL}}/{{VERSION}}/*
	api_v1 := router.Group(fmt.Sprintf("/api/v%d", VERSION))
	api_v1.GET("fizzbuzz/:int1/:int2/:limit/:str1/:str2", fizzbuzz.Validator, statistics.Monitoring, fizzbuzz.GetFizzBuzz)
	api_v1.GET("statistics", statistics.GetStatistics)

	var err error = nil
	if *secure {
		gin.SetMode(gin.ReleaseMode)
		err = router.RunTLS(fmt.Sprintf("%s:%s", *host, *port), *certificate, *key)
	} else {
		err = router.Run(fmt.Sprintf("%s:%s", *host, *port))
	}
	if err != nil {
		log.Fatalf("Cannot run server: %s\n", err)
	}
}
