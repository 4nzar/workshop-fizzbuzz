/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   fizzbuzz_controller.go                                                   */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 06:26:51 by 4nzar                                    */
/*   Updated: 20/03/2023 16:06:41 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package fizzbuzz

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

/*
** \parameters
**   ctx *gin.Context
** \description
**   Bind URI value to struct and pass it to the function fizzbuzz
** \return
**   In success, it will return a Json containing an array of string generated
**   by fizzbuzz with the values passed as paramaters else it
**   will return an error and abort the request.
 */

// @Tags fizzbuzz
// @Summary  Perform FizzBuzz operation given specific parameters.
// @Param int1 path int true "A number whose multiples will be replaced by str1." minimum(1) maximum(100)
// @Param int2 path int true "A number whose multiples will be replaced by str2." minimum(1) maximum(100)
// @Param limit path int true "The maximum of range where FizzBuzz will operate. Must be between 1 & 100." minimum(1) maximum(100)
// @Param str1 path string true "String that will replace all multiples of int1"
// @Param str2 path string true "String that will replace all multiples of int2."
// @Success 200 {array} string "FizzBuzz result based on the data passed on parameters"
// @failure 400 {object} ErrorResponse "bad request"
// @Router /api/v1/fizzbuzz/{int1}/{int2}/{limit}/{str1}/{str2} [get]
func GetFizzBuzz(ctx *gin.Context) {
	params := GetFizzBuzzRequest{}
	err := ctx.BindUri(&params)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		ctx.JSON(
			http.StatusBadRequest,
			ErrorResponse{http.StatusBadRequest, err.Error()},
		)
		return
	}
	result := FizzBuzz(
		params.Int1,
		params.Int2,
		params.Limit,
		params.Str1,
		params.Str2,
	)
	ctx.JSON(http.StatusOK, result)
}
