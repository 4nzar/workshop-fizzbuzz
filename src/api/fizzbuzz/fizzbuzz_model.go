/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   fizzbuzz_model.go                                                        */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 07:53:03 by 4nzar                                    */
/*   Updated: 20/03/2023 07:53:29 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package fizzbuzz

type GetFizzBuzzRequest struct {
	Int1  int    `json:"int1" uri:"int1" binding:"required"`
	Int2  int    `json:"int2" uri:"int2" binding:"required"`
	Limit int    `json:"limit" uri:"limit" binding:"required"`
	Str1  string `json:"str1" uri:"str1" binding:"required"`
	Str2  string `json:"str2" uri:"str2" binding:"required"`
}

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
