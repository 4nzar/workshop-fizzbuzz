/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   fizzbuzz_middleware.go                                                   */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 07:51:04 by 4nzar                                    */
/*   Updated: 21/03/2023 08:22:05 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package fizzbuzz

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

/*
** @parameters
**   ctx *gin.Context
** @description
**   Check if parameters are valid, if valid it will call fizzbuzz else return
**   an error.
** @return
**   In success, it will continue else it
**   will return an error and abort the request.
 */

func Validator(ctx *gin.Context) {
	params := GetFizzBuzzRequest{}
	err := ctx.BindUri(&params)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		ctx.JSON(
			http.StatusBadRequest,
			ErrorResponse{http.StatusBadRequest, err.Error()},
		)
		ctx.Abort()
		return
	}
	if params.Limit > 100 || params.Limit < 1 {
		err = errors.New("limit out of bounds (range between 1-100)")
	} else if params.Int1 > params.Limit || params.Int1 <= 0 ||
		params.Int2 > params.Limit || params.Int2 <= 0 {
		err = fmt.Errorf("int1 and int2 value out of bounds (range: 1-%v)", params.Limit)
	} else if params.Int1 == params.Int2 {
		err = errors.New("int1 and int2 cannot be of the same value")
	} else if strings.Trim(params.Str1, " ") == "" || strings.Trim(params.Str2, " ") == "" {
		err = errors.New("str1 and str2 cannot be empty")
	} else if strings.Contains(params.Str1, "-") || strings.Contains(params.Str2, "-") {
		err = errors.New("str1 and str2 cannot contains a '-' character")
	} else if params.Str1 == params.Str2 {
		err = errors.New("str1 and str2 cannot be of the same value")
	}
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		ctx.JSON(
			http.StatusBadRequest,
			ErrorResponse{http.StatusBadRequest, err.Error()},
		)
		ctx.Abort()
		return
	}
	ctx.Next()
}
