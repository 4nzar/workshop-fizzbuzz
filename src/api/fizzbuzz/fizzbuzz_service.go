/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   fizzbuzz_service.go                                                      */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 06:13:34 by 4nzar                                    */
/*   Updated: 20/03/2023 08:03:55 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package fizzbuzz

import (
	"strconv"
)

/*
** @parameters
**   int1	int
**   int2	int
**   limit	int
**   str1	string
**   str2	string
** @description
**
** @return
**   return a list of string from 1 to limit, where all multiples of int1 are
** 	 replaced by str1, all multiples of int2 are replaced by str2, all multiples
**   of int1 and int2 are replaced by str1str2.
 */

func FizzBuzz(int1, int2, limit int, str1, str2 string) []string {
	result := []string{}
	for index := 1; index <= limit; index++ {
		if index%int1 == 0 && index%int2 == 0 {
			result = append(result, str1+str2)
		} else if index%int1 == 0 {
			result = append(result, str1)
		} else if index%int2 == 0 {
			result = append(result, str2)
		} else {
			result = append(result, strconv.Itoa(index))
		}
	}
	return result
}
