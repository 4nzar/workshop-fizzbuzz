/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   statistics_middleware.go                                                 */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 07:26:52 by 4nzar                                    */
/*   Updated: 20/03/2023 12:38:38 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package statistics

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/4nzar/workshop-fizzbuzz/src/api/fizzbuzz"
)

func Monitoring(ctx *gin.Context) {
	params := fizzbuzz.GetFizzBuzzRequest{}
	err := ctx.BindUri(&params)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		ctx.JSON(
			http.StatusBadRequest,
			fizzbuzz.ErrorResponse{
				Code:    http.StatusBadRequest,
				Message: err.Error(),
			},
		)
		ctx.Abort()
		return
	}
	member := fmt.Sprintf("%03d-%03d-%03d-%s-%s", params.Int1, params.Int2,
		params.Limit, params.Str1, params.Str2)
	hits, err := new_hits()
	if err != nil {
		ctx.JSON(
			http.StatusInternalServerError,
			fizzbuzz.ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			},
		)
		ctx.Abort()
		return
	}
	hits.increase_score_member(member)
	ctx.Next()
}
