/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   statistics_service.go                                                    */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 10:36:23 by 4nzar                                    */
/*   Updated: 20/03/2023 11:22:02 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package statistics

import (
	"errors"
	"strconv"
	"strings"

	"github.com/go-redis/redis"
)

func handle_hits(hits []redis.Z) (*GetStatisticsResponse, error) {
	if len(hits) == 0 {
		return nil, errors.New("no record")
	}
	if len(hits) > 1 && hits[0].Score == hits[1].Score {
		return nil, errors.New("several requests have the top hits")
	}
	str, ok := hits[0].Member.(string)
	if !ok {
		return nil, errors.New("record cannot be processed")
	}
	params := strings.Split(str, "-")
	int1, _ := strconv.Atoi(params[0])
	int2, _ := strconv.Atoi(params[1])
	limit, _ := strconv.Atoi(params[2])
	result := &GetStatisticsResponse{
		Hits:  int(hits[0].Score),
		Int1:  int1,
		Int2:  int2,
		Limit: limit,
		Str1:  params[3],
		Str2:  params[4],
	}
	return result, nil
}
