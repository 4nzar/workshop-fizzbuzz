/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   statistics_model.go                                                      */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 08:14:36 by 4nzar                                    */
/*   Updated: 20/03/2023 12:53:23 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package statistics

import (
	"errors"
	"fmt"
	"os"

	"github.com/go-redis/redis"
)

/* ************************************************************************** */
/*                                                                            */
/*                               STRUCTURE                                    */
/*                                                                            */
/* ************************************************************************** */

type GetStatisticsResponse struct {
	Int1  int    `json:"int1"`
	Int2  int    `json:"int2"`
	Limit int    `json:"limit"`
	Str1  string `json:"str1"`
	Str2  string `json:"str2"`
	Hits  int    `json:"hits"`
}

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type Hits struct{}

type Hit struct {
	Score  int    `json:"score"`
	Member string `json:"member"`
}

/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/* ************************************************************************** */

var client *redis.Client = redis.NewClient(&redis.Options{
	Addr:     fmt.Sprintf("%s:%d", os.Getenv("REDIS_HOSTNAME"), 6379),
	Password: "",
	DB:       0,
})

/* ************************************************************************** */
/*                                                                            */
/*                               METHODS                                      */
/*                                                                            */
/* ************************************************************************** */

func new_hits() (*Hits, error) {
	if client == nil {
		return nil,
			errors.New("an error occured during instantiation of the client")
	}

	return &Hits{}, nil
}

func (hs *Hits) increase_score_member(member string) {
	client.ZIncrBy(KEY, 1, member)
}

func (hs Hits) get_statistics() (*GetStatisticsResponse, error) {
	ok, err := client.Exists(KEY).Result()
	if err != nil {
		return nil, errors.New("database unavailable")
	} else if ok == 0 {
		return nil, errors.New("no Data can be found")
	}
	result, err := client.ZRevRangeWithScores(KEY, 0, -1).Result()
	if err != nil {
		return nil, err
	}
	response, err := handle_hits(result)
	if err != nil {
		return nil, err
	}
	return response, nil
}
