/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   statistics_controller.go                                                 */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 08:13:41 by 4nzar                                    */
/*   Updated: 20/03/2023 16:05:34 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package statistics

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	KEY = "hits-request"
)

// @Tags statistics
// @Summary Fetch the most frequent request on '/api/fizzbuzz', its parameters and number of hits.
// @Success 200 {object}  GetStatisticsResponse ""
// @failure 500 {object} ErrorResponse ""
// @Router /api/v1/statistics [get]
func GetStatistics(ctx *gin.Context) {
	hits, err := new_hits()
	if err != nil {
		ctx.JSON(
			http.StatusInternalServerError,
			ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			},
		)
		return
	}
	response, err := hits.get_statistics()
	if err != nil {
		ctx.JSON(
			http.StatusInternalServerError,
			ErrorResponse{
				Code:    http.StatusInternalServerError,
				Message: err.Error(),
			},
		)
		return
	}
	log.Println(response)
	ctx.JSON(http.StatusOK, response)
}
