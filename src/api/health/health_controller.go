/* ************************************************************************** */
/*                                                                            */
/*       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        */
/*       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       */
/*      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        */
/*      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       */
/*       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        */
/*      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       */
/*      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        */
/*       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       */
/*                                                                            */
/*   health_controller.go                                                     */
/*                                                                            */
/*   By: 4nzar <sadik.essaidi@gmail.com>                                      */
/*                                                                            */
/*   Created: 20/03/2023 14:50:10 by 4nzar                                    */
/*   Updated: 21/03/2023 09:35:18 by 4nzar                                    */
/*                                                                            */
/* ************************************************************************** */

package health

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Tags healthcheck
// @Summary health check route to verify if the service is down or not
// @Success 200 {string} string "Will return a ok, that tell us that the server is still running"
// @Router / [get]
func HealthCheck(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, "ok")
}
