# ############################################################################ #
#                                                                              #
#        _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___         #
#        _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|        #
#       |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_         #
#       |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|        #
#        _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___         #
#       | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|        #
#       |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_         #
#        _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|        #
#                                                                              #
#   Makefile                                                                   #
#                                                                              #
#   By: 4nzar <sadik.essaidi@gmail.com>                                        #
#                                                                              #
#   Created: 20/03/2023 08:39:22 by 4nzar                                      #
#   Updated: 21/03/2023 10:39:12 by 4nzar                                      #
#                                                                              #
# ############################################################################ #

NAME			= workshop
VERSION			= 1
ORCHESTRATOR	= docker-compose
CONFIG_DIR		= data/configuration

.PHONY: all
all: $(NAME)

$(NAME):
	@$(ORCHESTRATOR) \
		-f $(CONFIG_DIR)/$(ORCHESTRATOR).yml \
		-f $(CONFIG_DIR)/$(ORCHESTRATOR).prod.yml up -d --build

.PHONY: dev
dev: 
	@$(ORCHESTRATOR) -f $(CONFIG_DIR)/$(ORCHESTRATOR).yml up -d --build  

.PHONY: version
version:
	@echo $(NAME)-$(VERSION)

.PHONY: ps
ps:
	@$(ORCHESTRATOR) -f $(CONFIG_DIR)/$(ORCHESTRATOR).yml ps

.PHONY: clean
clean: 
	@$(ORCHESTRATOR) -f $(CONFIG_DIR)/$(ORCHESTRATOR).yml down

.PHONY: fclean
fclean: clean
	@docker image rm image-$(NAME)-api

.PHONY: re
re: fclean dev

